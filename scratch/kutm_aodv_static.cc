/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2009 The Boeing Company
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

//  Based on code https://github.com/neje/ns3-aodv-etx
//  Author: miralem.mehic@etf.unsa.ba

#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/mobility-module.h"
#include "ns3/config-store-module.h"
#include "ns3/wifi-module.h"
#include "ns3/internet-module.h"
#include "ns3/netanim-module.h"
#include "ns3/flow-monitor-module.h"
#include "ns3/aodv-module.h"
#include "ns3/aodvetx-module.h"
#include "ns3/applications-module.h"
#include "ns3/propagation-module.h"

#include <iostream>
#include <fstream>
#include <vector>
#include <string>

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("Ideal8nodes");


//Create variables to test the results 
uint32_t m_bytes_sent = 0; 
uint32_t m_bytes_received = 0; 

uint32_t m_packets_sent = 0; 
uint32_t m_packets_received = 0; 

//Create help variable m_time
double m_time = 0;

//Create c++ map for measuring delay time
std::map<uint32_t, double> m_delayTable;
 
/**
   * \brief Each time the packet is sent, this function is called using TracedCallback and information about packet is stored in m_delayTable
   * \param context - used to display full path of the information provided (useful for taking information about the nodes or applications)
   * \param packet - Ptr reference to the packet itself
   */
void
SentPacket(std::string context, Ptr<const Packet> p){
    
    /* 
    //HELP LINES USED FOR TESTING
    std::cout << "\n ..................SentPacket....." << p->GetUid() << "..." <<  p->GetSize() << ".......  \n";
    p->Print(std::cout);                
    std::cout << "\n ............................................  \n";  
    */

    //Sum bytes of the packet that was sent
    m_bytes_sent  += p->GetSize(); 
    m_packets_sent++;

    //Insert in the delay table details about the packet that was sent
    m_delayTable.insert (std::make_pair (p->GetUid(), (double)Simulator::Now().GetSeconds()));
}

void
ReceivedPacket(std::string context, Ptr<const Packet> p, const Address& addr){
    
    /*
    //HELP LINES USED FOR TESTING
    std::cout << "\n ..................ReceivedPacket....." << p->GetUid() << "..." <<  p->GetSize() << ".......  \n";
    p->Print(std::cout);                
    std::cout << "\n ............................................  \n";  
    */

    //Find the record in m_delayTable based on packetID
    std::map<uint32_t, double >::iterator i = m_delayTable.find ( p->GetUid() );
     
    //Remove the entry from the delayTable to clear the RAM memroy and obey memory leakage
    if(i != m_delayTable.end()){
        m_delayTable.erase(i);
    }

    //Sum bytes and number of packets that were sent
    m_bytes_received += p->GetSize(); 
    m_packets_received++;
}

void
ReceivedPacketCout(std::string context, Ptr<const Packet> p, const Address& addr){
    
    /*
    //HELP LINES USED FOR TESTING
    std::cout << "\n ..................ReceivedPacket....." << p->GetUid() << "..." <<  p->GetSize() << ".......  \n";
    p->Print(std::cout);                
    std::cout << "\n ............................................  \n";  
    */

    //Find the record in m_delayTable based on packetID
    std::map<uint32_t, double >::iterator i = m_delayTable.find ( p->GetUid() );
     
    //Display the delay for the packet in the form of "packetID delay" where delay is calculated as the current time - time when the packet was sent
    std::cout << p->GetUid() << "\t" << (double)Simulator::Now().GetSeconds() - i->second << "\n";

    //Remove the entry from the delayTable to clear the RAM memroy and obey memory leakage
    if(i != m_delayTable.end()){
        m_delayTable.erase(i);
    }

    //Sum bytes and number of packets that were sent
    m_bytes_received += p->GetSize(); 
    m_packets_received++;
}


void
Ratio(){

    std::cout << "Sent (bytes):\t" <<  m_bytes_sent
    << "\tReceived (bytes):\t" << m_bytes_received 
    << "\nSent (Packets):\t" <<  m_packets_sent
    << "\tReceived (Packets):\t" << m_packets_received 
    
    << "\nRatio (bytes):\t" << (float)m_bytes_received/(float)m_bytes_sent
    << "\tRatio (packets):\t" << (float)m_packets_received/(float)m_packets_sent << "\n";
}

int 
main (int argc, char *argv[])
{
  std::string phyMode ("DsssRate2Mbps");
  double txpDistance = 100;  // m
  uint32_t packetSize = 64; // bytes
  uint32_t numPackets = 10;
  double interval = 0.5; // seconds
  bool verbose = false;
  uint32_t nNodes = 8;
  std::string protocol = "ns3::UdpSocketFactory"; 
  uint16_t port = 80;
  uint32_t routingProtocol = 0;
  uint32_t txpdistance = 60; 
  uint32_t stream = 1;
  uint32_t showdelay = 0;
  uint32_t randomMobility = 0;
  double simulationTime = 250; 
  double nodeSpeed = 0;
  double pauseTime = 1.0;

  Packet::EnablePrinting(); 
  PacketMetadata::Enable ();

  CommandLine cmd;

  cmd.AddValue ("phyMode", "Wifi Phy mode", phyMode);
  cmd.AddValue ("txpDistance", "Specify node's transmit range [m], Default:100", txpDistance);
  cmd.AddValue ("packetSize", "size of application packet sent", packetSize);
  cmd.AddValue ("numPackets", "number of packets generated", numPackets);
  cmd.AddValue ("interval", "interval (seconds) between packets", interval);
  cmd.AddValue ("verbose", "turn on all WifiNetDevice log components", verbose);
  cmd.AddValue ("transport TypeId", "TypeId for socket factory", protocol);
  cmd.AddValue ("txpdistance", "Wifi Range", txpdistance);
  cmd.AddValue ("routingProtocol", "Routing protocol type (0-AODV; 1- AODV_ETX)", routingProtocol);
  cmd.AddValue ("simulationTime", "simulationTime", simulationTime);
  cmd.AddValue ("showdelay", "showdelay", showdelay); 
  cmd.AddValue ("randomMobility", "randomMobility", randomMobility);
  cmd.AddValue ("nodeSpeed", "nodeSpeed", nodeSpeed); 
  cmd.AddValue ("stream", "stream", stream);
  

  cmd.Parse (argc, argv);
  // Convert to time object
  Time interPacketInterval = Seconds (interval);

  // disable fragmentation for frames below 2200 bytes
  Config::SetDefault ("ns3::WifiRemoteStationManager::FragmentationThreshold", StringValue ("2200"));
  // turn off RTS/CTS for frames below 2200 bytes
  Config::SetDefault ("ns3::WifiRemoteStationManager::RtsCtsThreshold", StringValue ("2200"));
  // Fix non-unicast data rate to be the same as that of unicast
  Config::SetDefault ("ns3::WifiRemoteStationManager::NonUnicastMode", StringValue (phyMode));

  // Fix non-unicast data rate to be the same as that of unicast
  Config::SetDefault ("ns3::aodv::RoutingProtocol::PrintRoutingTable", UintegerValue (1));
  Config::SetDefault ("ns3::aodvetx::RoutingProtocol::PrintRoutingTable", UintegerValue (1));

  ns3::RngSeedManager::SetSeed(100);
  RngSeedManager::SetRun (stream); 

  NodeContainer c;
  c.Create (nNodes);


  // Note that with RangePropagationLossModel, the positions below are not 
  // used for received signal strength. 

  if(nodeSpeed == 0){

    MobilityHelper mobility;
    Ptr<ListPositionAllocator> positionAlloc = CreateObject<ListPositionAllocator> ();
    positionAlloc->Add (Vector (200, 200, 3.0));    //0
    positionAlloc->Add (Vector (10, 50, 3.0));   //1
    positionAlloc->Add (Vector (0, 100, 3.0));   //2
    positionAlloc->Add (Vector (55, 90, 3.0));  //3
    positionAlloc->Add (Vector (60, 40, 3.0));   //4

    positionAlloc->Add (Vector (80, 80, 3.0));  //5
    positionAlloc->Add (Vector (100, 50, 3.0));  //6
    positionAlloc->Add (Vector (140, 55, 3.0)); //7
    mobility.SetPositionAllocator (positionAlloc);
    mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
    mobility.Install (c);

  }else{
    
    MobilityHelper adhocMobility;
    ObjectFactory pos;
    pos.SetTypeId ("ns3::RandomRectanglePositionAllocator");
    pos.Set ("X", StringValue ("ns3::UniformRandomVariable[Min=0.0|Max=1000.0]"));
    pos.Set ("Y", StringValue ("ns3::UniformRandomVariable[Min=0.0|Max=1000.0]"));
    Ptr<PositionAllocator> taPositionAlloc = pos.Create ()->GetObject<PositionAllocator> ();

    std::ostringstream speedUniformRandomVariableStream;
    speedUniformRandomVariableStream << "ns3::UniformRandomVariable[Min=0.0|Max="
                                     << nodeSpeed
                                     << "]";

    std::ostringstream pauseConstantRandomVariableStream;
    pauseConstantRandomVariableStream << "ns3::ConstantRandomVariable[Constant="
                                      << pauseTime
                                      << "]";

    adhocMobility.SetMobilityModel ("ns3::RandomWaypointMobilityModel",
                                    "Speed", StringValue (speedUniformRandomVariableStream.str ()),
                                    "Pause", StringValue (pauseConstantRandomVariableStream.str ()),
                                    "PositionAllocator", PointerValue (taPositionAlloc)
                                    );
    adhocMobility.Install (c); 
  }

  /*
  // Create propagation loss matrix
  Ptr<MatrixPropagationLossModel> lossModel = CreateObject<MatrixPropagationLossModel> ();
  lossModel->SetDefaultLoss (0); // set default loss to 200 dB (no link)

  lossModel->SetLoss (c.Get (1)->GetObject<MobilityModel>(), c.Get (4)->GetObject<MobilityModel>(), 100); // loss 1 <-> 4
  lossModel->SetLoss (c.Get (1)->GetObject<MobilityModel>(), c.Get (2)->GetObject<MobilityModel>(), 40); // loss 1 <-> 2
  lossModel->SetLoss (c.Get (2)->GetObject<MobilityModel>(), c.Get (3)->GetObject<MobilityModel>(), 100); // loss 2 <-> 3
  lossModel->SetLoss (c.Get (3)->GetObject<MobilityModel>(), c.Get (5)->GetObject<MobilityModel>(), 100); // loss 3 <-> 5
  lossModel->SetLoss (c.Get (5)->GetObject<MobilityModel>(), c.Get (6)->GetObject<MobilityModel>(), 40); // loss 5 <-> 6
  lossModel->SetLoss (c.Get (3)->GetObject<MobilityModel>(), c.Get (4)->GetObject<MobilityModel>(), 40); // loss 3 <-> 4
  lossModel->SetLoss (c.Get (4)->GetObject<MobilityModel>(), c.Get (5)->GetObject<MobilityModel>(), 100); // loss 4 <-> 5
  lossModel->SetLoss (c.Get (4)->GetObject<MobilityModel>(), c.Get (6)->GetObject<MobilityModel>(), 10); // loss 4 <-> 6
  lossModel->SetLoss (c.Get (6)->GetObject<MobilityModel>(), c.Get (7)->GetObject<MobilityModel>(), 10); // loss 6 <-> 7
  */

  // The below set of helpers will help us to put together the wifi NICs we want
  WifiHelper wifi;
  if (verbose)
    {
      wifi.EnableLogComponents ();  // Turn on all Wifi logging
    }
  wifi.SetStandard (WIFI_PHY_STANDARD_80211b);

  YansWifiPhyHelper wifiPhy =  YansWifiPhyHelper::Default ();
  // ns-3 supports RadioTap and Prism tracing extensions for 802.11b
  wifiPhy.SetPcapDataLinkType (YansWifiPhyHelper::DLT_IEEE802_11_RADIO); 

  // Create & setup wifi channel
  YansWifiChannelHelper wifiChannel;
  wifiChannel.SetPropagationDelay ("ns3::ConstantSpeedPropagationDelayModel");
  wifiChannel.AddPropagationLoss ("ns3::RangePropagationLossModel", "MaxRange", DoubleValue (txpdistance));
  Ptr<YansWifiChannel> ch = wifiChannel.Create (); 
  //ch->SetPropagationLossModel (lossModel);
  wifiPhy.SetChannel (ch);

  // Add a mac and disable rate control
  WifiMacHelper wifiMac;
  wifi.SetRemoteStationManager (
    "ns3::ConstantRateWifiManager", 
    "DataMode", StringValue (phyMode), 
    "ControlMode", StringValue (phyMode)
  );

  // Set it to adhoc mode
  wifiMac.SetType ("ns3::AdhocWifiMac");
  NetDeviceContainer devices = wifi.Install (wifiPhy, wifiMac, c);

  //HERE WE DEFINE TCP/IP protocol stack and define routing protocol to be used
  InternetStackHelper internet;
  if(routingProtocol == 0){
    AodvHelper aodv;
    internet.SetRoutingHelper(aodv);
  }else{
    AodvetxHelper aodvEtx;
    internet.SetRoutingHelper(aodvEtx);
  }
  internet.Install (c);

  Ipv4AddressHelper ipv4;
  NS_LOG_INFO ("Assign IP Addresses.");
  ipv4.SetBase ("192.168.1.0", "255.255.255.0");
  Ipv4InterfaceContainer i = ipv4.Assign (devices);

  // Soket
  TypeId tid = TypeId::LookupByName (protocol);
  InetSocketAddress destinationAddress = InetSocketAddress (Ipv4Address ("192.168.1.8"), port);
  InetSocketAddress sinkAddress = InetSocketAddress (Ipv4Address::GetAny (), port); 

  // Applications
  OnOffHelper onOff (protocol, destinationAddress);
  onOff.SetConstantRate (DataRate ("10Mbps"));
  ApplicationContainer serverApps = onOff.Install (c.Get (1));
  serverApps.Start (Seconds (15.0));
  serverApps.Stop (Seconds (80.0));

  PacketSinkHelper sink (protocol, sinkAddress);
  ApplicationContainer clientApps = sink.Install (c.Get (7));
  clientApps.Start (Seconds (15.0));
  clientApps.Stop (Seconds (80.0));


  std::cout << "Source IP address: 192.168.1.2" << std::endl;
  std::cout << "Destination IP address: 192.168.1.8"  << std::endl;

  // Tracing 
  Config::Connect ("/NodeList/*/ApplicationList/*/$ns3::OnOffApplication/Tx", MakeCallback (&SentPacket));
  if(showdelay == 0){
    Config::Connect ("/NodeList/*/ApplicationList/*/$ns3::PacketSink/Rx", MakeCallback (&ReceivedPacket));
  }else{
    Config::Connect ("/NodeList/*/ApplicationList/*/$ns3::PacketSink/Rx", MakeCallback (&ReceivedPacketCout));
  }
  

  wifiPhy.EnablePcap ("etf-floor", devices);
  
  // Output what we are doing
  NS_LOG_UNCOND ("Testing ...");

  // Flow monitor
  FlowMonitorHelper flowMonHlp;
  Ptr <FlowMonitor> flowMon = flowMonHlp.InstallAll ();
  
  // NetAnim simulator
  AnimationInterface anim ("netanim.xml");
  //anim.EnablePacketMetadata (true);
  anim.SetMobilityPollInterval (Seconds (1));
  
  // Output config store to txt/xml format
  Config::SetDefault ("ns3::ConfigStore::Filename", StringValue ("attributes.txt"));
  Config::SetDefault ("ns3::ConfigStore::FileFormat", StringValue ("RawText"));
  //Config::SetDefault ("ns3::ConfigStore::Filename", StringValue ("etf-floor-attributes.xml"));
  //Config::SetDefault ("ns3::ConfigStore::FileFormat", StringValue ("Xml"));
  Config::SetDefault ("ns3::ConfigStore::Mode", StringValue ("Save"));
  ConfigStore outputConfig;
  outputConfig.ConfigureDefaults ();
  outputConfig.ConfigureAttributes ();  
  
  Simulator::Stop (Seconds (simulationTime));
  Simulator::Run ();
  
  Ratio();
  
  flowMon -> SerializeToXmlFile ("flomon.xml", true, true);
  Simulator::Destroy ();

  return 0;
}

