
#ifdef NS3_MODULE_COMPILATION
# error "Do not include ns3 module aggregator headers from other modules; these are meant only for end user scripts."
#endif

#ifndef NS3_MODULE_AODVETX
    

// Module headers:
#include "aodvetx-dpd.h"
#include "aodvetx-helper.h"
#include "aodvetx-id-cache.h"
#include "aodvetx-neighbor-etx.h"
#include "aodvetx-neighbor.h"
#include "aodvetx-packet.h"
#include "aodvetx-routing-protocol.h"
#include "aodvetx-rqueue.h"
#include "aodvetx-rtable.h"
#endif
