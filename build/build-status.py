#! /usr/bin/env python

# Programs that are runnable.
ns3_runnable_programs = ['build/scratch/ns3.29-kutm_aodv_static-debug', 'build/scratch/ns3.29-kutm_aodv-debug', 'build/scratch/subdir/ns3.29-subdir-debug', 'build/scratch/ns3.29-scratch-simulator-debug']

# Scripts that are runnable.
ns3_runnable_scripts = []

