set terminal png
set output "AODV_vs_AODVETX_delay.png"
set title "AODV Delay"
set xlabel "Packet"
set ylabel "Time (second)"

set border linewidth 2 
set style line 1 linecolor rgb 'red' linetype 1 linewidth 1
set style line 2 linecolor rgb 'blue' linetype 2 linewidth 1
set grid ytics  
set grid xtics
set key right top
plot "delay_aodv.dat" title "AODV Delay" with linespoints  ls 1, "delay_aodv_etx.dat" title "AODV (ETX) Delay" with linespoints  ls 2

